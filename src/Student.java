public class Student {
    String name;
    float grade;

    public Student(String name, float grade) {
        this.name = name;
        this.grade = grade;
    }

    public String getName(){
        return name;
    }

    public float getGrade() {
        return grade;
    }

    // чтобы студенты выводились без дополнительного кода
    @Override
    public String toString(){
        return "Фамилия: " + this.getName() + "; оценка: " + this.getGrade();
    }
}
