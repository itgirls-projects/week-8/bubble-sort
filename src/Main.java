import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        float minGrade = 2.0f;
        float maxGrade = 5.0f;

        ArrayList<Student> studentArrayList = new ArrayList<>();
        studentArrayList.add(new Student("Иванова", gradeGenerator(minGrade, maxGrade)));
        studentArrayList.add(new Student("Сидорова", gradeGenerator(minGrade, maxGrade)));
        studentArrayList.add(new Student("Козлова", gradeGenerator(minGrade, maxGrade)));
        studentArrayList.add(new Student("Петухова", gradeGenerator(minGrade, maxGrade)));
        studentArrayList.add(new Student("Марьишкина", gradeGenerator(minGrade, maxGrade)));
        studentArrayList.add(new Student("Петрова", gradeGenerator(minGrade, maxGrade)));

        for (Student student : studentArrayList) {
            System.out.println(student);
        }
        System.out.println("--------");
        System.out.println("Отсортированный список:");
        bubbleSort(studentArrayList);
        for (Student student : studentArrayList) {
            System.out.println(student);
        }
    }

    // генерация случайных отметок для студентов
    public static float gradeGenerator(float min, float max) {
        Random random = new Random();
        float grade = random.nextFloat() * (max - min) + min;
        return roundGrade(1, grade);
    }

    // округление отметок
    public static float roundGrade(int decimalPlace, float grade) {
        BigDecimal finalGrade = new BigDecimal(Float.toString(grade));
        finalGrade = finalGrade.setScale(decimalPlace, RoundingMode.UP);
        return finalGrade.floatValue();
    }

    public static void bubbleSort(ArrayList<Student> arrayList) {
        for (int j = 0; j < arrayList.size() - 1; j++) {
            for (int i = 0; i < arrayList.size() - j - 1; i++) {
                if (arrayList.get(i).getGrade() > arrayList.get(i + 1).getGrade()) {
                    Student swap = arrayList.get(i + 1);
                    arrayList.set(i + 1, arrayList.get(i));
                    arrayList.set(i, swap);
                }
            }
        }
    }
}
